<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(
	'prestashop_api_description' => 'Ce plugin permet de configurer la clé nécessaire pour accéder à l’API de Webservice d’un site utilisant Prestashop et offre un itérateur et des fonctions pour faciliter l’intégration d’éléments de Prestashop dans les squelettes SPIP.',
	'prestashop_api_nom' => 'Liaison avec l’API de Prestashop',
	'prestashop_api_slogan' => 'Afficher des éléments issus d’un site sous Prestashop',
);
