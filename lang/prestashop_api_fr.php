<?php
if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(


	// C
	'categorie' => 'Catégorie',
	'categories' => 'Catégories',
	'cfg_parametrages_explications' => 'Dans votre Prestashop, «Paramètres avancées > Services Web», créez ou réutilisez une clé de webservice.',
	'cfg_titre_parametrages' => 'Paramétrages',

	// I
	'info_1_produit' => 'Un produit',
	'info_nb_produits' => '@nb@ produits',

	// L
	'logo' => 'Logo',
	'label_url_prestashop' => 'URL de la boutique Prestashop',
	'label_cle_webservice' => 'Clé d’accès au Webservice',

	// P
	'prestashop_api_titre' => 'Prestashop Webservice API',
	'prix' => 'Prix',
	'produit' => 'Produit',
	'produits' => 'Produits',
	'product_acheter' => 'Acheter ce produit',

	// T
	'titre_page_configurer_prestashop_api' => 'Configurer l’accès au Webservice Prestashop',
	'titre_page_demo_prestashop_api' => 'Tester l’accès au Webservice Prestashop',

);
